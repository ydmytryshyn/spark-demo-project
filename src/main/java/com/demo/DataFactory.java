package com.demo;

import org.apache.spark.sql.Dataset;
import org.apache.spark.sql.Row;
import org.apache.spark.sql.SparkSession;
import org.apache.spark.sql.types.StructType;

import java.util.List;

public class DataFactory {

    private final SparkSession sparkSession;

    public DataFactory(SparkSession sparkSession) {
        this.sparkSession = sparkSession;
    }

    public Dataset<Row> getDataFromList(List<Row> listOfRows, StructType schema) {
        return sparkSession.createDataFrame(listOfRows, schema);
    }

    public Dataset<Row> getDataFromCsvFile(String filePath, StructType schema) {
        //TODO
        return null;
    }
}
