package com.demo;

import org.apache.log4j.Level;
import org.apache.log4j.Logger;
import org.apache.spark.sql.Dataset;
import org.apache.spark.sql.Row;
import org.apache.spark.sql.RowFactory;
import org.apache.spark.sql.SparkSession;
import org.apache.spark.sql.types.DataTypes;
import org.apache.spark.sql.types.Metadata;
import org.apache.spark.sql.types.StructField;
import org.apache.spark.sql.types.StructType;
import org.junit.jupiter.api.BeforeAll;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.spark_project.guava.collect.ImmutableList;

import java.io.Serializable;
import java.util.List;

import static org.apache.spark.sql.functions.col;
import static org.apache.spark.sql.functions.desc;
import static org.apache.spark.sql.functions.lit;
import static org.junit.jupiter.api.Assertions.assertEquals;

class TestDemoClass implements Serializable {

    private SparkSession spark;

    @BeforeAll
    static void initSystem() {
        //System.setProperty("hadoop.home.dir", "c:/program_files/hadoop");
        Logger.getLogger("org").setLevel(Level.ERROR);
    }

    @BeforeEach
    void initSparkSession() {
        spark = SparkSession
                .builder()
                .appName("TestDemoClass")
                .master("local")
                .config("spark.driver.allowMultipleContexts", "false")
                .config("spark.sql.shuffle.partitions", "8")
                //.config("spark.testing.memory", "2147480000") // On x32 system
                .getOrCreate();

        spark.sparkContext().setLogLevel("ERROR");

    }

    @Test
    void testMethod() {

        final String[] inputColNames = new String[]{"id", "value"};

        final List<Row> dataInput = ImmutableList.of(
                RowFactory.create(1, 25.5),
                RowFactory.create(2, -4.2),
                RowFactory.create(3, 3.8),
                RowFactory.create(4, 1.9),
                RowFactory.create(5, 7.0),
                RowFactory.create(7, 0.5),
                RowFactory.create(6, 10.5)
        );

        final StructType schema = new StructType(new StructField[]{
                new StructField(inputColNames[0], DataTypes.IntegerType, false, Metadata.empty()),
                new StructField(inputColNames[1], DataTypes.DoubleType, false, Metadata.empty()),
        });

        final DataFactory dataFactory = new DataFactory(spark);

        final Dataset<Row> datasetInput = dataFactory.getDataFromList(dataInput, schema).repartition(4);
        datasetInput.show();

        datasetInput.createOrReplaceTempView("test_tbl");

        final double TH = 2.0;

        final Dataset<Row> result1 = spark.sql(
                "select " +
                        "  id, " +
                        "  value " +
                        "from test_tbl " +
                        "where value >= " + Double.toString(TH) + " " +
                        "order by value desc"
        );
        result1.show();

        final Dataset<Row> result2 = datasetInput.select("id", "value")
                .where(col("value").geq(lit(TH)))
                .orderBy(desc("value"));
        result2.show();

        assertEquals(result1.count(), result2.count());

    }

}